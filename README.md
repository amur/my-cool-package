# First Steps in Python Packaging

Welcome! This repo is a skeleton of a Python package to get you familiar with
packaging. You are meant to "fill in the blanks" to get this package
working properly.

## Pre-Requisites

This code uses Python 3.6.


## Your Goal

Try to get the `test_pypack.py` file to run without errors _and_ produce
results that make sense. :)
